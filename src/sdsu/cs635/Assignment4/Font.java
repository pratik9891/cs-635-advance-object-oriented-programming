package sdsu.cs635.Assignment4;

public class Font {
	private java.awt.Font aFont;
	private String name;
	private int style;
	private int size;

	public Font(String name, int style, int size) {
		aFont = new java.awt.Font(name, style, size);
		this.name = name;
		this.style = style;
		this.size = size;
	}

	public boolean equals(String name, int style, int size) {
		return (this.name.equals(name)) && (this.style == style)
				&& (this.size == size);
	}
}
