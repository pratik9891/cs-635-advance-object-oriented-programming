package sdsu.cs635.Assignment4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CharacterFactoryTest {

	CharacterFactory testCharacterFatory;
	Character firstA, firstB, secondA, secondB;

	@Before
	public void setup() {
		System.out.printf("Size of Character factory %.1f bytes%n",
				new SizeofUtil() {
					@Override
					protected int create() {
						testCharacterFatory = CharacterFactory.instance();
						firstA = testCharacterFatory.getCharacter('a');
						firstB = testCharacterFatory.getCharacter('b');
						secondA = testCharacterFatory.getCharacter('a');
						secondB = new Character('b');
						return 1;
					}

					protected void reset() {
						CharacterFactory.instance().reset();
					}
				}.averageBytes());
	}

	@Test
	public void test() {
		assertTrue(firstA == secondA);
		assertFalse(firstB == secondB);
	}

}
