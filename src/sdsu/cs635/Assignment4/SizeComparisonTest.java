package sdsu.cs635.Assignment4;

/*
 * Copyright (c) 2011.  Peter Lawrey
 *
 * "THE BEER-WARE LICENSE" (Revision 128)
 * As long as you retain this notice you can do whatever you want with this stuff.
 * If we meet some day, and you think this stuff is worth it, you can buy me a beer in return
 * There is no warranty.
 */

import org.junit.Test;

public class SizeComparisonTest {

	String a = "CS 635 Advanced Object-Oriented Design & Programming Spring Semester, 2014 Doc 19 Facade & Mediator April 24, 2014 Copyright ??, All rights reserved. 2014 SDSU & Roger Whitney, 5500 Campanile Drive, San Di-ego, CA 92182-7700 USA. OpenContent (http://www.opencontent.org/opl.shtml) license de-fines the copyright on this document.";
	char[] charList = a.toCharArray();
	double sizeUsingFlyweigt = 0;
	double sizeUsingNormalMethod = 0;

	@Test
	public void testHeaderSize() {
		sizeUsingFlyweigt = new SizeofUtil() {
			Character[] result = null;
			CharacterFactory characterFactory = null;
			RunArray<Font> runArray = null;
			FontFactory fontFactory = null;

			@Override
			protected int create() {
				result = new Character[charList.length];
				characterFactory = CharacterFactory.instance();
				runArray = new RunArray<Font>();
				fontFactory = FontFactory.instance();
				runArray.addRun(0, 150, fontFactory.getFont("Times", java.awt.Font.BOLD, 11));
				runArray.addRun(150, charList.length,
						fontFactory.getFont("Courier", java.awt.Font.ITALIC, 12));
				for (int i = 0; i < charList.length; i++)
					result[i] = characterFactory.getCharacter(charList[i]);
				return 1;
			}
			protected void reset(){
				CharacterFactory.instance().reset();
				FontFactory.instance().reset();
			}
		}.averageBytes();

		// Normal Way
		sizeUsingNormalMethod = new SizeofUtil() {
			Character[] chars = null;
			java.awt.Font[] fonts = null;

			@Override
			protected int create() {
				chars = new Character[charList.length];
				fonts = new java.awt.Font[charList.length];
				for (int i = 0; i < charList.length; i++) {
					chars[i] = new Character(charList[i]);
					fonts[i] = new java.awt.Font("Times", java.awt.Font.BOLD,
							12);
				}
				return 1;
			}
			protected void reset(){
				
			}
		}.averageBytes();
		System.out.println("Number of Characters in the document: " + a.length());
		
		System.out.printf(
				"Size usage when using Flyweight pattern is %.1f bytes%n",
				sizeUsingFlyweigt);
		System.out.printf(
				"Size usage when using Normal method is %.1f bytes%n",
				sizeUsingNormalMethod);
		System.out.println("Percetage saving in sapce: "+(1 - (sizeUsingFlyweigt/sizeUsingNormalMethod))*100 + "%");
	}
}
