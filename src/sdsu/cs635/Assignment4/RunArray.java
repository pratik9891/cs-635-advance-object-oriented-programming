package sdsu.cs635.Assignment4;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class RunArray<E> {

	ArrayList<Range> runs;
	ArrayList<E> values;
	int currentMaxRange;

	public RunArray() {
		runs = new ArrayList<Range>();
		values = new ArrayList<E>();
		currentMaxRange = -1;
	}

	public void addRun(int startIndex, int runLength, E value) {
		runs.add(new Range(startIndex, startIndex + runLength));
		values.add(value);
		currentMaxRange = startIndex + runLength -1;
	}
	
	public void appendRun(int runLength, E value){
		runs.add(new Range(currentMaxRange+1, currentMaxRange+1+runLength));
		values.add(value);
		currentMaxRange = currentMaxRange+1+runLength;
	}
	
	public E getValueAt(int index) throws NoSuchElementException{
		E desiredValue = null;
		for(int i=0; i<runs.size(); i++){
			if(runs.get(i).contains(index))
				desiredValue =  values.get(i);
		}
		if(desiredValue == null)
			throw new NoSuchElementException();
		return desiredValue;
	}

	private class Range {
		private int lowerLimit, upperLimit;

		Range(int lowerLimit, int upperLimit) {
			this.lowerLimit = lowerLimit;
			this.upperLimit = upperLimit-1;
		}

		public boolean contains(int value) {
			return value >= lowerLimit && value <= upperLimit;
		}
	}
}
