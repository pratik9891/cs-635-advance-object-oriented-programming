package sdsu.cs635.Assignment4;

import java.util.HashMap;
import java.util.Map;

public class CharacterFactory {

	private Map<java.lang.Character, Character> characterPool;

	private CharacterFactory() {
		characterPool = new HashMap<java.lang.Character, Character>();
	}

	private static class SingletonHolder {
		private static CharacterFactory instance = new CharacterFactory();
	}

	public void reset() {
		SingletonHolder.instance = null;
	}

	public static CharacterFactory instance() {
		if (SingletonHolder.instance == null)
			SingletonHolder.instance = new CharacterFactory();
		return SingletonHolder.instance;
	}

	public Character getCharacter(java.lang.Character charKey) {
		if (!characterPool.containsKey(charKey))
			characterPool.put(charKey, new Character(charKey));
		return characterPool.get(charKey);
	}

	public static void main(String[] args) {
		java.lang.Character input = new java.lang.Character('a');
		System.out.println(input.getClass());
	}
}
