package sdsu.cs635.Assignment4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RunArrayTest {
	
	RunArray<String> runArrayTest;
	RunArray<Integer> directAppendTest;
	
	
	@Before
	public void setup(){
		runArrayTest = new RunArray<String>();
		runArrayTest.addRun(0, 150, "A");
		runArrayTest.addRun(150, 10, "B");
		runArrayTest.appendRun(10, "C");
		
		directAppendTest = new RunArray<Integer>();
		directAppendTest.appendRun(10, 2);
		directAppendTest.addRun(10, 10, 99);
	}

	@Test
	public void test() {
		assertTrue("A" == runArrayTest.getValueAt(149));
		assertTrue("B" == runArrayTest.getValueAt(159));
		assertTrue("B" == runArrayTest.getValueAt(150));
		assertTrue("C" == runArrayTest.getValueAt(160));
		assertTrue("C" == runArrayTest.getValueAt(169));
		
		assertTrue(2 == directAppendTest.getValueAt(9));
		assertTrue(99 == directAppendTest.getValueAt(10));
		assertTrue(99 == directAppendTest.getValueAt(19));
	}

}
