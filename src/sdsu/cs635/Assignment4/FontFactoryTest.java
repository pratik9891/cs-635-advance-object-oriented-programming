package sdsu.cs635.Assignment4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FontFactoryTest {

	FontFactory testFontFactory;
	Font firstTimes, secondTimes;

	@Before
	public void setup() {
		System.out.printf("Size of font factory %.10f bytes%n",
				new SizeofUtil() {
					@Override
					protected int create() {
						testFontFactory = FontFactory.instance();
						firstTimes = testFontFactory.getFont("Times",
								java.awt.Font.BOLD, 11);
						secondTimes = testFontFactory.getFont("Times",
								java.awt.Font.BOLD, 11);
						return 1;
					}

					protected void reset() {
						FontFactory.instance().reset();
					}
				}.averageBytes());
	}

	@Test
	public void test() {
		assertTrue(firstTimes == secondTimes);

	}

}
