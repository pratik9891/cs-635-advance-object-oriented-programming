package sdsu.cs635.Assignment4;

import java.util.ArrayList;
import java.util.Collection;

public class FontFactory {

	private Collection<Font> fontPool;

	private FontFactory() {
		fontPool = new ArrayList<Font>();
	}

	private static class SingletonHolder {
		private static FontFactory instance = new FontFactory();
	}

	public void reset() {
		SingletonHolder.instance = null;
	}

	public static FontFactory instance() {
		if (SingletonHolder.instance == null)
			SingletonHolder.instance = new FontFactory();
		return SingletonHolder.instance;
	}

	public Font getFont(String name, int style, int size) {
		if (fontPool.size() != 0) {
			for (Font each : fontPool)
				if (each.equals(name, style, size))
					return each;
		}

		Font currentFont = new Font(name, style, size);
		fontPool.add(currentFont);
		return currentFont;
	}

}
