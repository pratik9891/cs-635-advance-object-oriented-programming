package sdsu.cs635.InMemoryDatabase;

import java.io.Serializable;

/**
 * @author Pratik Sharma
 * 
 * 		   Provides a lazy and thread safe singleton counter
 * 		   Implementation Due to Bill Pugh!
 * 
 */

public class Counter implements Serializable{
	
	private int count = 0;
	private Counter() {}
	
	private static class SingletonHolder {
		private final static Counter INSTANCE = new Counter();
	}
	
	public static Counter instance(){
		return SingletonHolder.INSTANCE;
	}
	
	public int increase(){
		return ++count;
	}

}
