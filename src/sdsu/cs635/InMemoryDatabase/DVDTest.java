package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class DVDTest {
	
	private DVD testDVD, anotherDVD, testDVDCopy;
	
	@Before
	public void setupDVDTest(){
		testDVD = new DVD();
		testDVD.setName("Matrix");
		testDVD.setPrice(new BigDecimal("3.99"));
		testDVD.setQuantity(50);
		
		anotherDVD = new DVD();
		anotherDVD.setName("Star Wars");
		anotherDVD.setPrice(new BigDecimal("9.99"));
		anotherDVD.setQuantity(42);		
		
		//reset the counter, to check equality.
		Counter reset = Counter.instance();
		Class c = reset.getClass();
		try {
			Field field = c.getDeclaredField("count");
			field.setAccessible(true);
			field.set(reset, new Integer(0));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();		
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		testDVDCopy = new DVD();
		testDVDCopy.setName("Matrix");
		testDVDCopy.setPrice(new BigDecimal("3.99"));
		testDVDCopy.setQuantity(50);
	}

	@Test
	public void test() {
		assertEquals("Matrix", testDVD.getName());
		assertEquals(1, testDVD.getID());
		assertEquals(new BigDecimal("3.99") , testDVD.getPrice());
		assertEquals(50, testDVD.getQuantity());
		
		assertEquals(2, anotherDVD.getID());
		assertFalse(testDVD.equals(anotherDVD));
		
		assertTrue(testDVD.equals(testDVDCopy));
	}

}
