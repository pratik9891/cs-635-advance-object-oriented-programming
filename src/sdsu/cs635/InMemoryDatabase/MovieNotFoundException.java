package sdsu.cs635.InMemoryDatabase;


public class MovieNotFoundException extends InventoryException {
	
	public MovieNotFoundException(){
		super();
	}
	
	public MovieNotFoundException(String s){
		super(s);
	}
	
}
