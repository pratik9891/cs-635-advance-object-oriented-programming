package sdsu.cs635.InMemoryDatabase;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.ArrayList;

public class Command {
	
	private String commandString;
	private Object[] arguments;
	
	public Command(String command, Object[] arguments){
		this.arguments = arguments;	
		this.commandString = command; 
	}
	
	public String getCommand(){
		return commandString;
	}
	
	public Object[] getArguments(){
		return arguments;
	}
	
	public void execute(Object receiver){
		Method command = null;
		Class cls = receiver.getClass();
		Class[] argumentTypes = new Class[arguments.length];
		for (int i=0; i < arguments.length; i++)
            argumentTypes[i] = arguments[i].getClass();
		
		try {
			command = cls.getMethod(commandString, argumentTypes);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		try {
			command.invoke(receiver, arguments);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	
}
