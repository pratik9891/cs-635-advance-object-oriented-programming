package sdsu.cs635.InMemoryDatabase;

import java.math.BigDecimal;

public abstract class InventoryDecorator implements Inventory {

	protected MovieInventory movieInventory;

	public InventoryDecorator(MovieInventory inventory) {
		this.movieInventory = inventory;
	}

	public void add(String name) {
		movieInventory.add(name);
	}

	public void sell(String name, Integer quantity) {
		try {
			movieInventory.sell(name, quantity);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		} catch (InsufficientQuantityException e) {
			e.printStackTrace();
		}
	}

	public void updatePrice(String name, BigDecimal newPrice) {
		try {
			movieInventory.updatePrice(name, newPrice);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void updateQuantity(String name, Integer quantity) {
		try {
			movieInventory.updateQuantity(name, quantity);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
	}

	public BigDecimal getPrice(String name) {
		BigDecimal result = null;
		try {
			result = movieInventory.getPrice(name);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public BigDecimal getPrice(Integer id) {
		BigDecimal result = null;
		try {
			result = movieInventory.getPrice(id);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Integer getQuantity(String name) {
		Integer result = null;
		try {
			result = movieInventory.getQuantity(name);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Integer getQuantity(Integer id) {
		Integer result= null;
		try {
			result =  movieInventory.getQuantity(id);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

}
