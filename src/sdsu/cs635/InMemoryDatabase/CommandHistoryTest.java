package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommandHistoryTest {
	
	MovieInventory testInventory, restoredInventory;
	ArrayList<Command> commands, restoredCommands;
	CommandHistory commandHistory;
	
	@Before
	public void setupCommandHistroyTest(){
		commandHistory = new CommandHistory();
		commands = new ArrayList<Command>();
		testInventory = new MovieInventory();
		restoredInventory = new MovieInventory();
		Object[] addArguments = {"Star Wars"};
		Command addCommand = new Command("add", addArguments);
		commands.add(addCommand);
		Object[] updatePriceArguments = {"Star Wars", new BigDecimal("10.00")};
		Command updatePriceCommand = new Command("updatePrice", updatePriceArguments);
		commands.add(updatePriceCommand);
		Object[] updateQuantityArguments = {"Star Wars", 5};
		Command updateQuantityCommand = new Command("updateQuantity", updateQuantityArguments);
		commands.add(updateQuantityCommand);
		for(Command each: commands){
			each.execute(testInventory);
			commandHistory.write(each);
		}
		
		restoredCommands = commandHistory.read();
		for(Command each: restoredCommands)
			each.execute(restoredInventory);
	}

	@Test
	public void test() {
		assertTrue(testInventory.size() == restoredInventory.size());
		assertTrue(restoredCommands.size() == commands.size());
	}
	
	@After
	public void testCleanup(){
		commandHistory.deleteAll();
	}
	
	

}
