package sdsu.cs635.InMemoryDatabase;

public class InventoryException extends Exception{
	
	public InventoryException(){
		super();
	}

	public InventoryException(String s){
		super(s);
	}
	
}
