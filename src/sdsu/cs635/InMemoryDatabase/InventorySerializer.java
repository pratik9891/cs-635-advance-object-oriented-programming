package sdsu.cs635.InMemoryDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class InventorySerializer {

	InventoryMemento memento;
	private File storage, backupStorage;

	public InventorySerializer() {
		storage = new File(InventoryMemento.class.getSimpleName() + ".ser");
		backupStorage = new File(storage.getName() + ".backup");
	}
	
	public void addMemento(InventoryMemento memento){
		FileOutputStream fileStream = null;
		ObjectOutputStream oos = null;
		try{
			if(storage.exists()){
				Files.copy(storage.toPath(), backupStorage.toPath());
			}
			fileStream = new FileOutputStream(storage);
			oos = new ObjectOutputStream(fileStream);
			oos.writeObject(memento);
			oos.close();
			backupStorage.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public InventoryMemento getMemento(){
		InventoryMemento inventoryFormFile = null;
		FileInputStream fileStream = null;
		ObjectInputStream ois = null;
		if(storage.exists() && backupStorage.exists()){
			try {
				Files.copy(backupStorage.toPath(), storage.toPath());
				Files.delete(backupStorage.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(storage.exists()){
			try{
				fileStream = new FileInputStream(storage);
				ois = new ObjectInputStream(fileStream);
				inventoryFormFile = (InventoryMemento) ois.readObject();
				ois.close();
			} catch (FileNotFoundException e){
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			} catch (ClassNotFoundException e){
				e.printStackTrace();
			}
		}
		
		return inventoryFormFile;
	}
	
	public void deleteAll(){
		if(storage.exists())
			storage.delete();
		if(backupStorage.exists())
			backupStorage.delete();
	}
}
