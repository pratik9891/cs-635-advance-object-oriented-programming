package sdsu.cs635.InMemoryDatabase;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Formatter;

/**
 * @author Pratik Sharma
 * 
 *         Stores a DVD's four attributes :- 
 *          Name (String) 
 *          ID (Auto incremented,starts with a zero) 
 *          Quantity (number of DVD's of the same type) 
 *          Price (price in dollars, stored as a BigDecimal to avoid precision loss)
 * 
 */

public class DVD implements Serializable {

	private String name;
	private BigDecimal price;
	private Integer id;
	private Counter counter;
	private Integer quantity;

	public DVD() {
		counter = Counter.instance();
		id = counter.increase();
		quantity = 0;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getID() {
		return id;
	}

	public boolean equals(Object o) {
		boolean result = false;
		if (o == this)
			result = true;
		if (!(o instanceof DVD))
			result = false;
		else {
			DVD otherDVD = (DVD) o;
			if (this.name.equals(otherDVD.getName())
					&& this.id.equals(otherDVD.getID())
					&& this.price.equals(otherDVD.getPrice())
					&& this.quantity.equals(otherDVD.getQuantity()))
				result = true;
		}
		return result;

	}
	
	public String toString(){
		String result;
		result = this.id.toString();
		return result;
	}


}
