package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class CommandTest {
	
	Command testCommand;
	MovieInventory inventory, commandTestInventory;
	
	@Before
	public void setupCommandtest(){
		inventory = new MovieInventory();
		inventory.add("Star Wars");
		try {
			inventory.updatePrice("Star Wars", new BigDecimal("10.00"));
			inventory.updateQuantity("Star Wars", 10);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		
		//Resets the counter
		Counter reset = Counter.instance();
		Class c = reset.getClass();
		try {
			Field field = c.getDeclaredField("count");
			field.setAccessible(true);
			field.set(reset, new Integer(0));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();		
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		commandTestInventory = new MovieInventory();
		Object[] addArguments = {"Star Wars"};
		Command addCommand= new Command("add", addArguments);
		addCommand.execute(commandTestInventory);
		Object[] updatePriceArguments = {"Star Wars", new BigDecimal("10.00")};
		Command updatePriceCommand = new Command("updatePrice", updatePriceArguments);
		updatePriceCommand.execute(commandTestInventory);
		Object[] updateQuantityArguments = {"Star Wars", 10};
		Command updateQuantityCommand = new Command("updateQuantity", updateQuantityArguments);
		updateQuantityCommand.execute(commandTestInventory);
		
	}

	@Test
	public void test() {
		try {
			assertEquals(inventory.getPrice("Star Wars"), commandTestInventory.getPrice("Star Wars"));
			assertEquals(inventory.getPrice(1),commandTestInventory.getPrice(1));
			assertEquals(inventory.getQuantity(1), commandTestInventory.getQuantity(1));
			assertEquals(inventory.getQuantity("Star Wars"), commandTestInventory.getQuantity("Star Wars"));
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		assertEquals(inventory.size(), commandTestInventory.size());
	}

}
