package sdsu.cs635.InMemoryDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;

public class CommandHistory {

	private File storage, backupStorage;

	public CommandHistory() {
		storage = new File(Command.class.getSimpleName() + ".json");
		backupStorage = new File(storage.getName() + ".backup");
	}

	public void write(Command c) {
		try {
			if (storage.exists()) {
				Files.copy(storage.toPath(), backupStorage.toPath());
			}
			FileWriter writer = new FileWriter(storage, true);
			writer.write(c.getCommand() + ",");
			for (Object each : c.getArguments())
				writer.write(each + ",");
			writer.write("\n");
			writer.close();
			backupStorage.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Command> read() {
		ArrayList<Command> restoredCommands = new ArrayList<Command>();
		if (storage.exists() && backupStorage.exists()) {
			try {
				Files.copy(backupStorage.toPath(), storage.toPath());
				Files.delete(backupStorage.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedReader br = new BufferedReader(new FileReader(storage));
			String line;
			String[] commandObject;

			while ((line = br.readLine()) != null) {
				line.replace(" ", "");
				commandObject = line.split(",");
				Object[] arguments = new Object[commandObject.length - 1];
				for (int i = 1; i < commandObject.length; i++)
					arguments[i - 1] = (Object) commandObject[i];
				if (commandObject[0].equals("updateQuantity")) {
					arguments[1] = (Integer.parseInt((String) arguments[1]));
				} else if (commandObject[0].equals("sell")) {
					arguments[1] = (Integer.parseInt((String) arguments[1]));
				} else if (commandObject[0].equals("updatePrice")) {
					arguments[1] = new BigDecimal((String) arguments[1]);
				}
				restoredCommands.add(new Command(commandObject[0], arguments));
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return restoredCommands;
	}

	public void deleteAll() {
		if (storage.exists())
			storage.delete();
		if (backupStorage.exists())
			backupStorage.delete();
	}
	
}
