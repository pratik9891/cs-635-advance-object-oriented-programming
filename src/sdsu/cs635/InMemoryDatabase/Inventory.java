package sdsu.cs635.InMemoryDatabase;

import java.math.BigDecimal;

public interface Inventory {

	public void add(String name);
	public void sell(String name, Integer sellQuantity) throws InventoryException, InsufficientQuantityException;
	public void updatePrice(String name, BigDecimal price) throws InventoryException;
	public void updateQuantity(String name, Integer quantity) throws InventoryException;
	public BigDecimal getPrice(String name) throws InventoryException;
	public BigDecimal getPrice(Integer id) throws InventoryException;
	public Integer getQuantity(String name) throws InventoryException;
	public Integer getQuantity(Integer id) throws InventoryException;
	
}
