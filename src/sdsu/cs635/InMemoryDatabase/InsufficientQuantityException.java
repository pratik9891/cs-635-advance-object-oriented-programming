package sdsu.cs635.InMemoryDatabase;

public class InsufficientQuantityException extends Exception{
	
	public InsufficientQuantityException(){
		super();
	}
	
	public InsufficientQuantityException(String s){
		super(s);
	}

}
