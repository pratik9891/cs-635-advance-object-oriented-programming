package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MovieInventoryHelperTest {
	
	MovieInventoryHelper beforeCrashHelper, afterCrashHelper;
	
	@Before
	public void setupMovieInventoryHelperTest(){
		beforeCrashHelper = new MovieInventoryHelper();
		for(int i= 1; i<7; i++){
			beforeCrashHelper.add("Star Wars"+i);
			beforeCrashHelper.updatePrice("Star Wars"+i, new BigDecimal("9.99"));
			beforeCrashHelper.updateQuantity("Star Wars"+i, 4);
			if(i%2 == 0)
				beforeCrashHelper.sell("Star Wars"+i, 1);
		}
			
		afterCrashHelper = new MovieInventoryHelper();
		afterCrashHelper.restore();
	}

	@Test
	public void test() {
		assertEquals(beforeCrashHelper.size(), afterCrashHelper.size());
		assertTrue(3 == afterCrashHelper.getQuantity("Star Wars2"));
		assertTrue(4 == afterCrashHelper.getQuantity("Star Wars1"));
	}
	
	@After
	public void testCleanup(){
		beforeCrashHelper.deleteAllBackup();
	}

}
