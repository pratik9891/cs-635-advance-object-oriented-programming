package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class MovieInventoryTest {
	
	MovieInventory testInventory;
	
	@Before
	public void setupMovieInventoryTest(){
		testInventory = new MovieInventory();
		testInventory.add("Swordfish");
		testInventory.add("Matrix");
		try {
			testInventory.updatePrice("Swordfish", new BigDecimal("9.99"));
			testInventory.updateQuantity("Swordfish", 10);
			testInventory.updatePrice("Matrix", new BigDecimal("11.99"));
			testInventory.updateQuantity("Matrix", 42);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}	
	}
	

	@Test
	public void test() {
		try {
			assertEquals(1, testInventory.getID("Swordfish"));
			assertEquals(new Integer(10), testInventory.getQuantity(1));
			assertEquals(new Integer(10), testInventory.getQuantity("Swordfish"));
			assertEquals(2, testInventory.getID("Matrix"));
			assertEquals(new Integer(42), testInventory.getQuantity(2));
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			testInventory.sell("Swordfish", 10);
			assertEquals(new Integer(0), testInventory.getQuantity(1));
			testInventory.sell("Swordfish", 1);
			fail("Should have thrown insufficientQuantityException");
		} catch (MovieNotFoundException | InsufficientQuantityException e1) {
			return;
		}
		
		
		try {
			testInventory.getPrice("Star Wars");
			fail("Should have thrown exception!");
		} catch (MovieNotFoundException e) {
			return;
		}

	}

}
