package sdsu.cs635.InMemoryDatabase;

import java.io.Serializable;
import java.util.Hashtable;

public class InventoryMemento implements Serializable{

	private Hashtable<String, DVD> state;
	
	public InventoryMemento() {
		state = new Hashtable<String, DVD>();
	}
	
	void setState(Hashtable<String, DVD> list){
		this.state = list;
	}
	
	Hashtable<String, DVD> getState() {
		return state;
	}
	
	
}
