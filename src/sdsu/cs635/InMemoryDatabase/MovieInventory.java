package sdsu.cs635.InMemoryDatabase;

import java.math.BigDecimal;
import java.util.Hashtable;

public class MovieInventory implements Inventory {

	Hashtable<String, DVD> list = new Hashtable<String, DVD>();

	public InventoryMemento createMemento() {
		InventoryMemento currentState = new InventoryMemento();
		currentState.setState(list);
		return currentState;
	}

	public void restoreMemento(InventoryMemento oldState) {
		if (oldState != null)
			list = oldState.getState();
	}

	public void add(String name) {
		DVD movie = new DVD();
		movie.setName(name);
		list.put(name, movie);
	}

	public void sell(String name, Integer sellQuantity)
			throws MovieNotFoundException, InsufficientQuantityException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		int currentQuantity = requestedMovie.getQuantity();
		if (currentQuantity >= sellQuantity)
			requestedMovie.setQuantity(currentQuantity - sellQuantity);
		else
			throw new InsufficientQuantityException();
	}

	public void updateQuantity(String name, Integer newQuantity)
			throws MovieNotFoundException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		int currentQuantity = requestedMovie.getQuantity();
		requestedMovie.setQuantity(currentQuantity + newQuantity);
	}

	public void updatePrice(String name, BigDecimal newPrice)
			throws MovieNotFoundException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		requestedMovie.setPrice(newPrice);
	}

	public BigDecimal getPrice(String name) throws MovieNotFoundException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		return requestedMovie.getPrice();
	}

	public Integer getQuantity(String name) throws MovieNotFoundException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		return requestedMovie.getQuantity();
	}

	public Integer getQuantity(Integer id) throws MovieNotFoundException {
		DVD requiredDVD = null;
		for (String each : list.keySet()) {
			if (list.get(each).getID() == id)
				requiredDVD = list.get(each);
		}
		if (requiredDVD == null)
			throw new MovieNotFoundException();
		return requiredDVD.getQuantity();
	}

	public BigDecimal getPrice(Integer id) throws MovieNotFoundException {
		DVD requiredDVD = null;
		for (String each : list.keySet()) {
			if (list.get(each).getID() == id)
				requiredDVD = list.get(each);
		}
		if (requiredDVD == null)
			throw new MovieNotFoundException();
		return requiredDVD.getPrice();
	}

	public String toString() {
		String result = "";
		for (String each : list.keySet())
			result += list.get(each).toString() + ",";
		return result;
	}

	public int getID(String name) throws MovieNotFoundException {
		if (!list.containsKey(name))
			throw new MovieNotFoundException();
		DVD requestedMovie = list.get(name);
		return requestedMovie.getID();
	}

	public int size() {
		return list.size();
	}

	public static void main(String[] args) {
		InventorySerializer c = new InventorySerializer();
		MovieInventory i = new MovieInventory();
		i.add("Star-wars");
		System.out.println("Before:" + i);
		c.addMemento(i.createMemento());
		i.add("batman");
		System.out.println("Second:" + i);
		i.restoreMemento(c.getMemento());
		System.out.println("Third:" + i);
	}

}
