package sdsu.cs635.InMemoryDatabase;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventorySerializerTest {

	MovieInventory testInventory;
	InventorySerializer testSerializer;

	@Before
	public void setupInventorySerializerTest() {
		testInventory = new MovieInventory();
		testSerializer = new InventorySerializer();
		testInventory.add("Star Wars");
		try {
			testInventory.updatePrice("Star Wars", new BigDecimal("9.99"));
			testInventory.updateQuantity("Star Wars", 42);
		} catch (MovieNotFoundException e) {
			e.printStackTrace();
		}
		testSerializer.addMemento(testInventory.createMemento());
		testInventory.add("Batman");

	}

	@Test
	public void test() {
		assertEquals(2, testInventory.size());
		testInventory.restoreMemento(testSerializer.getMemento());
		assertEquals(1, testInventory.size());
		try {
			testInventory.getID("Batman");
			fail("shoud have threw Movie not found exception");
		} catch (MovieNotFoundException e) {
			return;
		}

	}
	
	@After
	public void testCleanup(){
		testSerializer.deleteAll();
	}

}
