package sdsu.cs635.InMemoryDatabase;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MovieInventoryHelper extends InventoryDecorator {
	
	int numOperations = 0;
	CommandHistory commandbackup;
	InventorySerializer fileRestorer;
	
	public MovieInventoryHelper(MovieInventory inventory){
		super(inventory);
		commandbackup = new CommandHistory();
		fileRestorer = new InventorySerializer();
	}
	
	public MovieInventoryHelper(){
		this(new MovieInventory());
	}
	
	public void add(String name){
		checkState();
		Object[] arguments = {name};
		Command addCommand = new Command("add", arguments);
		addCommand.execute(super.movieInventory);	
		commandbackup.write(addCommand);
	}

	public void sell(String name, Integer quantity){
		checkState();
		Object[] arguments = {name, quantity};
		Command sellCommand = new Command("sell", arguments);
		sellCommand.execute(super.movieInventory);
		commandbackup.write(sellCommand);
	}
	
	public void updatePrice(String name, BigDecimal newPrice){
		checkState();
		Object[] arguments = {name, newPrice};
		Command updatePriceCommand = new Command("updatePrice", arguments);
		updatePriceCommand.execute(super.movieInventory);
		commandbackup.write(updatePriceCommand);
	}
	
	public void updateQuantity(String name, int newQuantity){
		checkState();
		Object[] arguments = {name, newQuantity};
		Command updateQuantityCommand = new Command("updateQuantity", arguments);
		updateQuantityCommand.execute(super.movieInventory);
		commandbackup.write(updateQuantityCommand);
	}
	
	public void restore(){
		MovieInventory backup = new MovieInventory();
		backup.restoreMemento(fileRestorer.getMemento());
		super.movieInventory = backup;
		ArrayList<Command> previousCommands = commandbackup.read();
		for(Command each: previousCommands)
			each.execute(super.movieInventory);
	}
	
	private void checkState(){
		if(numOperations == 5){
			forceSave();
		}
		numOperations++;
	}
	
	public void forceSave(){
		fileRestorer.addMemento(super.movieInventory.createMemento());
		numOperations = 0;
		commandbackup.deleteAll();
	}
	
	public int size(){
		return super.movieInventory.size();
	}
	
	public void deleteAllBackup(){
		commandbackup.deleteAll();
		fileRestorer.deleteAll();
	}
	
	
	public static void main(String[] args){
//		MovieInventory db = new MovieInventory();
//		MovieInventoryHelper dbHelper = new MovieInventoryHelper(db);
//		for(int i= 1; i<7; i++)
//			dbHelper.add("Star Wars"+i);
//		System.out.println( dbHelper.toString());
		MovieInventoryHelper restoreCheck = new MovieInventoryHelper();
		restoreCheck.restore();
		System.out.println(restoreCheck.toString());
	}

}
