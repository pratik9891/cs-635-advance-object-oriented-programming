package sdsu.cs635.Heap;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ HeapTest.class, IngFilterTest.class, NodeTest.class,
		IngHeapDecoratorTest.class })
public class AllTests {

}
