package sdsu.cs635.Heap;

import java.util.AbstractCollection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * @author Pratik Sharma
 * 
 *         An Unbounded Min-Max Heap based on the Node Data-Structure. The
 *         elements are ordered according to their Comparable natural ordering
 *         (Min-Heap) by default, or by providing reverse
 *         Comparator(Collections.reverseOrder()) at Heap creation time. The
 *         Heap does not permit adding non-comparable objects.
 * 
 *         The iterator provided by the Heap traverses the heap in In-order
 *         traversal.
 * 
 * 
 * @param <E>
 */
public class Heap<E> extends AbstractCollection<E> {

	private Node<E> root;
	private int size;

	/**
	 * The number of times this priority queue has been structurally modified.
	 */
	private int modCount;

	/**
	 * The comparator, or DefaultComparator if Heap uses elements natural
	 * ordering.
	 */
	private final Comparator<? super E> comparator;

	public Heap() {
		this((Comparator<? super E>) new DefaultComparator());
	}

	public Heap(Comparator<? super E> defaultComparator) {
		size = 0;
		root = LeafNode.getInstance();
		this.comparator = defaultComparator;
	}

	public boolean isEmpty() {
		return root.isLeaf();
	}

	@Override
	public boolean add(E data) {
		if (data == null)
			throw new NullPointerException();
		modCount++;
		size++;
		if (isEmpty()) {
			root = new HeapNode<E>(data, comparator);
		} else {
			root.add(data);
		}
		return true;
	}

	/**
	 * Returns an iterator over the elements in this queue. The iterator returns
	 * elements in Inorder.
	 */
	@Override
	public Iterator<E> iterator() {
		return new Itr();
	}

	private final class Itr implements Iterator<E> {

		private int expectedModCount;
		Stack<Node<E>> pendingNodeTraversal = new Stack<Node<E>>();

		private void populateLeftChildren(Node<E> current) {
			while (!current.isLeaf()) {
				pendingNodeTraversal.push(current);
				current = current.getLeft();
			}
		}

		Itr() {
			expectedModCount = modCount;
			populateLeftChildren(root);
		}

		@Override
		public boolean hasNext() {
			return !pendingNodeTraversal.isEmpty();
		}

		@Override
		public E next() {
			if (!hasNext())
				throw new NoSuchElementException();
			if (expectedModCount != modCount)
				throw new ConcurrentModificationException();
			Node<E> Node = pendingNodeTraversal.pop();
			populateLeftChildren(Node.getRight());

			return Node.getValue();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	@Override
	public String toString() {
		return root.toString();
	}

	/**
	 * Returns an array containing all of the elements in this Heap; the runtime
	 * type of the returned array is that of the specified array. The returned
	 * array elements are in Inorder.
	 */

	@Override
	public <E> E[] toArray(E[] o) {
		return super.toArray(o);
	}

	@Override
	public int size() {
		return size;
	}

}
