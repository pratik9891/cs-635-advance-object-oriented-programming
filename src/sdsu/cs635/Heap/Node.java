package sdsu.cs635.Heap;

/**
 * @author Pratik Sharma
 * 
 *         Provides the necessary methods that need to be overridden for a
 *         binary Node.
 * @param <E>
 */
public interface Node<E> {

	public int getTreeHeight();

	public String toString();

	public boolean isLeaf();

	public void add(E data);

	public void add(E data, Node<E> parent);

	public Node<E> getLeft();

	public Node<E> getRight();

	public E getValue();
}
