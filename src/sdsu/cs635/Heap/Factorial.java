package sdsu.cs635.Heap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;
 
 public class Factorial {
 
         public static void main(String[] args) throws NumberFormatException, IOException{
        	 BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        //         Scanner input = new Scanner(System.in);
                int counter = Integer.parseInt(input.readLine());
                 int i=0;
                 while (i<counter){
                         int number = Integer.parseInt(input.readLine());
                         System.out.println(Factorial.factorialZeros(number));
                         i++;
                 }
         }
 
         public static int factorialZeros(int number){
                 int result = 0;
                 int fiveDivisor =5;
                 while(((int)number/fiveDivisor) >= 1){
                        result += (int)number/fiveDivisor;
                        fiveDivisor *= 5;
                 }
                 return result;
         }
         
}

