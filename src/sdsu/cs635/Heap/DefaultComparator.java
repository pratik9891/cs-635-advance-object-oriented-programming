package sdsu.cs635.Heap;

import java.util.Comparator;

/**
 * @author Pratik Sharma
 * 
 *         DefaultComparator provides natural ordering of elements. For
 *         reverseOder, use Collections.reverseOrder
 * 
 * @param <T>
 */
public class DefaultComparator<T> implements Comparator<Comparable<Object>> {

	/**
	 * Returns a negative integer, zero, or a positive integer as the first
	 * argument is less than, equal to, or greater than the second.
	 */

	@Override
	public int compare(Comparable<Object> o1, Comparable<Object> o2) {
		return o1.compareTo(o2);
	}

}
