package sdsu.cs635.Heap;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class IngFilterTest {

	private Heap<String> testHeap;
	private IngFilter testIngFilter;
	private Collection<String> emptyTestCollection;

	@Before
	public void setupIngFilterTest() {
		testHeap = new Heap<String>();
		testHeap.add("aging");
		testHeap.add("bring");
		testHeap.add("drink");
		testHeap.add("zoo");
		testHeap.add("grand");
		testHeap.add("learning");	
		
		testIngFilter = new IngFilter(testHeap.iterator());
		
		emptyTestCollection = new ArrayList<String>();
	}
	
	@Test
	public void test() {
		
		int j = 100;
		while(j>0){
			assertTrue(testIngFilter.hasNext());
			j--;
		}
		assertEquals("bring", testIngFilter.next());
		assertEquals("learning", testIngFilter.next());
		assertEquals("aging", testIngFilter.next());
		assertFalse(testIngFilter.hasNext());
		
		testIngFilter = new IngFilter(emptyTestCollection.iterator());
		assertFalse(testIngFilter.hasNext());
	}


}
