package sdsu.cs635.Heap;

import java.io.Serializable;
import java.util.Comparator;


public class Ordering{

	public static <T> Comparator<T> normalOrder() {
        return (Comparator<T>) NORMAL_ORDER;
    }
	
	public static <T> Comparator<T> reverseOrder() {
        return (Comparator<T>) REVERSE_ORDER;
    }
	
	private static final Comparator NORMAL_ORDER = new NormalComparator();
	
	private static final Comparator REVERSE_ORDER = new ReverseComparator();
	
	
	private static class NormalComparator<T> implements Comparator<Comparable<Object>>{

		 public int compare(Comparable<Object> c1, Comparable<Object> c2) {
			 return c1.compareTo(c2);
	     }
	 }
	
	private static class ReverseComparator<T> implements Comparator<Comparable<Object>>{

		 public int compare(Comparable<Object> c1, Comparable<Object> c2) {
			 return c2.compareTo(c1);
	     }
	 }
	
	
}
