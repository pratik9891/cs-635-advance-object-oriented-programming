package sdsu.cs635.Heap;

import java.util.AbstractCollection;
import java.util.Iterator;

/**
 * @author Pratik Sharma
 * 
 *         The FilterAbstractCollection contains a reference to an
 *         AbtsractCollection which it uses as a data source. The
 *         FilterAbstractCollection simply overrides iterator, toString, toArray
 *         and size of AbstractCollection which pass all requests to the
 *         contained data source. Subclasses of FilterAbstractCollection may
 *         override some of these methods and also provide additional methods.
 * 
 * @param <T>
 */
public abstract class FilterAbstractCollection<T> extends AbstractCollection<T> {

	protected AbstractCollection<T> realCollection;

	public FilterAbstractCollection(AbstractCollection<T> collection) {
		this.realCollection = collection;
	}

	@Override
	public Iterator<T> iterator() {
		return realCollection.iterator();
	}

	@Override
	public int size() {
		return realCollection.size();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return super.toArray(a);
	}

}
