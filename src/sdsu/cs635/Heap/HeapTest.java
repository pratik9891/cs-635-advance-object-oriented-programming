package sdsu.cs635.Heap;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class HeapTest {

	private Heap<String> testMinHeap;
	private Heap<String> testEmptyHeap;
	Iterator<String> testIterator;
	private String[] testArrayStorage;
	private Collection<Integer> testMaxHeap;
	

	@Before
	public void setupTestHeap() {
		testMinHeap = new Heap<String>();
		testMinHeap.add("aging");
		testMinHeap.add("bing");
		testMinHeap.add("sing");
		testIterator = testMinHeap.iterator();
		testArrayStorage = new String[testMinHeap.size()];
		testMinHeap.toArray(testArrayStorage);
		
		testMaxHeap = new Heap<Integer>(Collections.reverseOrder());
		testMaxHeap.add(100);
		testMaxHeap.add(1000);
		testMaxHeap.add(10000);
		
		testEmptyHeap = new Heap<String>();
	}

	@Test
	public void test() {
		
		assertFalse(testMinHeap.isEmpty());
		assertTrue(testMinHeap.size() == 3);
		assertEquals("((bing)aging(sing))", testMinHeap.toString());
		assertTrue(testArrayStorage.length == testMinHeap.size());
		
		int j= 100;
		while(j>0){
			assertTrue(testIterator.hasNext());
			j--;
		}
		
		j= 0;
		while(testIterator.hasNext()){
			assertEquals(testArrayStorage[j],testIterator.next());
			j++;
		}
		
		assertFalse(testIterator.hasNext());
		
		
		assertEquals("((100)10000(1000))",testMaxHeap.toString());
		
		
		assertTrue(testEmptyHeap.isEmpty());
		testIterator = testEmptyHeap.iterator();
		assertFalse(testIterator.hasNext());
	}

}
