package sdsu.cs635.Heap;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Pratik Sharma
 * 
 *         The class provides a filter on top of the iterator interface, to
 *         remove words which do not end with "ing". Accepts the any iterator.
 *         The output order of "ing" words is same as that of the original
 *         iterator order.
 */
public class IngFilter implements Iterator<String> {

	private Iterator<String> iterator;
	private String nextMatch;
	private boolean hasNext;

	public IngFilter(Iterator<String> iterator) {
		this.iterator = iterator;
		findNext();
	}

	private String findNext() {
		String oldMatch = nextMatch;
		while (iterator.hasNext()) {
			String currentValue = iterator.next();
			if (currentValue.toLowerCase().endsWith("ing")) {
				hasNext = true;
				nextMatch = currentValue;
				return oldMatch;
			}
		}
		hasNext = false;
		return oldMatch;
	}

	/**
	 * Only returns true if the original iterator has a word ending with "ing"
	 */
	@Override
	public boolean hasNext() {
		return hasNext;
	}

	/**
	 * Returns the next "ing" ending word.
	 */
	@Override
	public String next() {
		if (!hasNext) {
			throw new NoSuchElementException();
		}
		return findNext();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
