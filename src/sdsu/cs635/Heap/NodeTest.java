package sdsu.cs635.Heap;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

public class NodeTest {

	private Node<String> testStringNode;
	private Node<String> testNullNode;
	private Node<Integer> testIntegerNode;

	@Before
	public void setupTestNode() {
		testStringNode = new HeapNode("apple");
		testStringNode.add("bing");
		testStringNode.add("crying");
		testNullNode = LeafNode.getInstance();
		testIntegerNode = new HeapNode(1, Collections.reverseOrder());
		testIntegerNode.add(2);
		testIntegerNode.add(3);
		testIntegerNode.add(4);
		testIntegerNode.add(5);
	}
	
	/**
	 * tests are performed for
	 * 1)real node as well as Leaf node functionality offered by the Node interface
	 * 2)generic's of the Node class
	 * 3)min-max startegy using comparator
	 */
	@Test
	public void test() {
		
		assertFalse(testStringNode.isLeaf());
		assertTrue(testStringNode.getTreeHeight() == 2);
		String output = testStringNode.toString();
		assertEquals("((bing)apple(crying))", output);
		assertEquals("(bing)", testStringNode.getLeft().toString());
		assertEquals("(crying)", testStringNode.getRight().toString());
		assertEquals("apple", testStringNode.getValue());
		
		assertTrue(testNullNode.isLeaf());
		assertTrue(testNullNode.getTreeHeight() == 0);
		output = testNullNode.toString();
		assertEquals("", output);
		assertEquals(null, testNullNode.getLeft());
		assertEquals(null, testNullNode.getRight());
		assertEquals(null, testNullNode.getValue());
		
		assertEquals("(((1)3)5((2)4))", testIntegerNode.toString());
		
	}

}
