package sdsu.cs635.Heap;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class IngHeapDecoratorTest {
	
	private Heap<String> testHeap;
	private IngHeapDecorator testIngHeapDecorator;
	private Iterator<String> testIngHeapDecoratorIterator;
	
	
	@Before
	public void setupTestIngHeapDecoartor(){
		testHeap = new Heap<String>();
		testHeap.add("xing");
		testHeap.add("frying");
		testHeap.add("making");
		testHeap.add("foo");
		testHeap.add("bar");
		
		testIngHeapDecorator = new IngHeapDecorator(testHeap);
		testIngHeapDecoratorIterator = testIngHeapDecorator.iterator();
	}

	@Test
	public void test() {
		assertEquals(3,testIngHeapDecorator.size());
		assertEquals("[xing, frying, making]", testIngHeapDecorator.toString());
		assertEquals("xing", testIngHeapDecoratorIterator.next());
		assertTrue(testIngHeapDecoratorIterator.hasNext());
		assertEquals("frying", testIngHeapDecoratorIterator.next());
		assertTrue(testIngHeapDecoratorIterator.hasNext());
		assertEquals("making", testIngHeapDecoratorIterator.next());
		assertFalse(testIngHeapDecoratorIterator.hasNext());
	}

}
