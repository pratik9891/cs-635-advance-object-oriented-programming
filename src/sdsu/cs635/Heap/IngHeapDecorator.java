package sdsu.cs635.Heap;

import java.util.Iterator;
import java.lang.String;

/**
 * @author Pratik Sharma
 * 
 *         IngHeapDecorator adds functionality to a Heap, the ability to only
 *         consider "ing" ending words in a Heap. When the IngHeapDecorator is
 *         created, the underlying Heap is also created which provides the
 *         necessary functionality to interact with the original heap.
 */
public class IngHeapDecorator extends FilterAbstractCollection<String> {

	public IngHeapDecorator(Heap<String> decoratee) {
		super(decoratee);
	}

	public Iterator<String> iterator() {
		return new IngFilter(super.iterator());
	}

	@Override
	public int size() {
		int counter = 0;
		Iterator<String> itr = this.iterator();
		while (itr.hasNext()) {
			itr.next();
			counter++;
		}
		return counter;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public String[] toArray(String[] result) {
		return super.toArray(result);
	}

}
