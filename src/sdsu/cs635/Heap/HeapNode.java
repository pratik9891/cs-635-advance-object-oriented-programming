package sdsu.cs635.Heap;

import java.util.Comparator;

public class HeapNode<E> implements Node<E> {

	private Node<E> leftChild;
	private Node<E> rightChild;
	private E value;
	
	/**
	 * The comparator, or DefaultComparator if Node uses elements natural
	 * ordering.
	 */
	Comparator<? super E> comparator;

	HeapNode() {
		comparator = (Comparator<? super E>) new DefaultComparator();
	}

	HeapNode(E newData) {
		this(newData, (Comparator<? super E>) new DefaultComparator());
	}

	HeapNode(E newData, Comparator<? super E> comparator) {
		leftChild = LeafNode.getInstance();
		rightChild = LeafNode.getInstance();
		this.value = newData;
		this.comparator = comparator;
	}

	public boolean isLeaf() {
		return false;
	}

	/*
	 * The method accepts new data to be added to the node. It checks if the new
	 * data is compared to the current node based on the Comparator. Based on
	 * the comparison, it adds it to the shortest subtree. If the heights are
	 * equal, it favors the left subtree.
	 */

	public void add(E data) {
		this.add(data, this);
	}

	public void add(E data, Node<E> Parent) {
		if (comparator.compare(this.value, data) < 0) {
			if (compareSubtreeHeight(this) <= 0) {
				leftChild.add(data, this);
			} else {
				rightChild.add(data, this);
			}
		} else {
			E swap = this.value;
			this.value = data;
			this.add(swap, this);
		}
	}

	protected void addPrivate(E data) {
		if (compareSubtreeHeight(this) <= 0) {
			this.leftChild = new HeapNode<E>(data, comparator);
		} else {
			this.rightChild = new HeapNode<E>(data, comparator);
		}

	}

	/*
	 * Returns -1 is left subtree is shorter and 1 when right subtree is
	 * shorter.
	 */
	private int compareSubtreeHeight(HeapNode<E> root) {
		return (root.leftChild.getTreeHeight() <= root.rightChild
				.getTreeHeight()) ? -1 : 1;
	}

	@Override
	public int getTreeHeight() {
		return Math.max(leftChild.getTreeHeight(), rightChild.getTreeHeight()) + 1;
	}

	public String toString() {
		return "(" + leftChild + value + rightChild + ")";
	}

	@Override
	public Node<E> getLeft() {
		return this.leftChild;
	}

	@Override
	public Node<E> getRight() {
		return this.rightChild;
	}

	@Override
	public E getValue() {
		return value;
	}

}
