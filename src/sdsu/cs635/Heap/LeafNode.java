package sdsu.cs635.Heap;

/**
 * @author Pratik Sharma LeafNode inherits from the Node, to provide the
 *         functionality of a Null object. Only one instance of a null node is
 *         ever made, and all the refrences to a nullnode refer to the same
 *         instance
 * @param <E>
 */
public class LeafNode<E> implements Node<E> {

	private static final LeafNode singleton = new LeafNode();

	private LeafNode() {
	}

	public static <E> LeafNode getInstance() {
		return singleton;
	}

	public boolean isLeaf() {
		return true;
	}

	@Override
	public void add(E data, Node<E> parent) {
		((HeapNode<E>) parent).addPrivate(data);
	}

	@Override
	public int getTreeHeight() {
		return 0;
	}

	public String toString() {
		return "";
	}

	@Override
	public Node<E> getLeft() {
		return null;
	}

	@Override
	public Node<E> getRight() {
		return null;
	}

	@Override
	public E getValue() {
		return null;
	}

	public void add(E data) {
		Node<E> parent = new HeapNode<E>();
		this.add(data, parent);
	}

}
